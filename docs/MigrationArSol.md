
# Protocole de migration d'ArSol #


Objectif de passer sous un système *opensource* sans plus dépendre de licences 4D, faciliter les mises à jour et les développement, s'orienter vers le Web sémantique.

Voici toutes les étapes de transformation auxquelles ont été soumises les données de la base ArSol.

## 4D : export des données sources ##

1. Ouvrir 4D ArSol en XP (ferdinand)

2. se connecter à ArSol (archéolat x2)

3. Sélectionner Tous les sites, puis Valider

4. Passer en mode structure : touche ESC ; Fichier > Structure

5. Enregistrements > Liste des Tables > sélectionner la table à
exporter

6. Outils > Etats rapides > Destination > File

7. Champs : tout sélectionner (mais pas les tables liées !!!)

> note : toutefois, noter quelles sont les tables liées afin de penser à les exporter ensuite sans exception

8. Options : vérifier les délimiteurs : 9 (tabulation) et 10 (saut de ligne) ; ne pas mettre 13 (retour chariot) sauf si aucune données textuelles (commentaires, identifications, interprétations).

9. Exécuter

10. dupliquer le TXT et le renommer en CSV

### Tables non exportées ###

``AFQuantMob`` : table vide (une seule fiche : Verre à vitre ; nombre = 518)

``AFRelations`` : mise en place des relations pour « Le Stratifiant », TODO : analyser le fonctionnement

``AFRelationsAnalyse`` : historique de l'utilisation de AFRelations pour gérer les relations stratigraphiques.

``Archives`` : version pas à jour (vide), Importé depuis
<https://arsol.huma-num.fr> dans la table ``af18archive``

``Champs`` : un seul enregistrement : n°6 / id_cle_Archive/ type= « L 0 »

``Connections`` : table vide

``Constantes`` : chemins d'exports qui ne sont plus valides

``ConstantesWeb`` : ne contient que l'adresse du dossier web d'arsol sur le serveur c:\ArSol\arsolweb

``Couleurs`` : contient des codes couleurs

``Croquis`` : test depuis la tablette d'inclusion de croquis aux fiches d'US
(6 enregistrements = tests)

``Interarchive`` : version pas à jour, Importé depuis
<https://arsol.huma-num.fr> dans la table ``arcv_interarchive``

``USnoeud`` : table vide (champs = US ; Etage ; Colonne)

## NOTEPAD++ : gestion des retours chariots pour l'import ##

1. Dans notepad++ : ouvrir le fichier CSV

Si la table ne contient pas de champ
commentaire / interprétation / description pouvant potentiellement comporter
des retours chariots, aller directement à l'étape 5.

2. Affichage > Symboles spéciaux > Afficher les symboles de fin de
ligne

Doivent apparaître :

- ``LF`` pour les fins de ligne

- ``CRLF`` pour les retours-chariot DANS les champs

3. Rechercher/Remplacer ; cocher en dessous ``Mode étendu`` :
Rechercher ``\r\n`` ; Remplacer par ``###``

> note : cela permet de conserver les retours chariots internes d'un champ sans que cela soit considéré comme une fin d'enregistrement

4. Rechercher/Remplacer ; cocher en dessous ``Mode étendu`` :
Rechercher ``\n`` ; Remplacer par ``\r\n``

> note : cela permet d'avoir une notation standard pour les retours chariots

5. Convertir en UTF-8

6. sauvegarder le fichier

## PHPmyAdmin : importation dans mySQL ##

Choix de MariaDB en InnoDB pour avoir la structure la plus rigoureuse possible

1. sélectionner la base ``arsol``, puis onglet ``Importer``

2. sélectionner le fichier CSV ; options : colonnes = ``\t``, sans guillemets doubles ensuite ; première ligne = en-tête

3. Opération : renommer la table et la passer en **InnoDB**

4. Structure : indiquer quel est le champ index unique (clé primaire) + définir les index secondaires

5. création des liens : Concepteur > tirer les liens depuis la clé primaire vers la clé étrangère.

> /!\ Pour les fichiers volumineux, PHPmyAdmin analyse la
totalité des données pour attribuer le type à chaque colonne, ce qui peut conduire à une erreur de type *Time out*. Toutefois, la structure est alors créée, (après un réétalonnage des types de champs) il suffit donc souvent de refaire l'import du fichier en sélectionnant la table et en demandant d'ignorer 1 ligne dans les requêtes SQL (la ligne d'en-tête ne doit pas être importée). Pour les fichiers très volumineux, il peut être nécessaire de les tronçonner en plusieurs.

### Clés primaires manquantes ###

> /!\ **Erreur 1452** : probablement parce que des clés
existent dans la table secondaire alors qu'elles n'existent pas dans la table primaire. Requête pour identifier les clés manquantes :

    SELECT T1.*
    FROM
    af01stratigraphie AS T1
    LEFT JOIN af02faits AS T2
    ON T1.CoSiFait = T2.CoSiNuFai
    WHERE T2.CoSiNuFai IS NULL
    AND T1.CoSiFait IS NOT NULL

Quelques tests ont permis de voir que les fiches papiers n'existent pas. Comme il n'est pas envisageable de faire des recherches pour tous ces enregistrements manquants, il a été décidé de les créer dans un premier temps pour répondre aux exigences d'intégrité d'InnoDB, mais de mentionner en commentaire « fiche manquante », à charge du producteur de données de retrouver plus tard les informations manquantes.

Un script a été créé à cet effet : **missingpk.php** (paramétrages requis dans le fichier).

Script utilisé pour compléter les PK manquants dans : Faits, Murs, Sépultures, Structures...
Suivre les commentaires en début de script pour paramétrer correctement le processus de complétion des clés manquantes.

### Clés étrangères à 000000 ###

Autre problème : 4D génère des clés automatiquement à partir des numéros de clés secondaires saisies, sauf que si la clé reste à zéro, cela génère parfois une clé secondaire du type AJ000000, qui n'a pas de clé primaire correspondante (puisqu'il s'agit justement d'absence de clé). Il faut
donc vider cette information pour tous les champs en CoSi...

    UPDATE `af01stratigraphie`
    SET `CoSiAgrNum` = NULL
    WHERE `CoSiAgrNum` LIKE "__000000"

~~TODO : créer un script pour détecter les champs vides dans chaque table (donner le nombre d'occurrences non-vides).~~

Utiliser le script **analyse.php** (aucun paramétrage requis) pour détecter :
- les champs systématiquement vides (à supprimer)
- les champs ayant une faible pourcentage d'absence d'information et qui peuvent potentiellement être complété
- le nombre de vide / NULL / 0 pour homogénéiser l'information

## Problèmes détectés ##

Table **af01stratigraphie**

> Le champ Phase a été utilisé pour mettre des numéros d'Ensemble pour le site ZW (attention que pour les sites ZZ et ZR, il s'agit de vrais numéros d'ensembles).

~~TODO : migrer pour le site ZW, les clés de phase vers les numéros d'ensemble. Mais résoudre les clés étrangères avant.~~

    UPDATE `af01stratigraphie`
    SET `CoSiEnsNum`= CONCAT('ZW',LPAD(`PhaseNum`,6,'0'))
    WHERE `SITE` = "ZW"

> Il existe aussi des numéros de phases pour les sites ZY et AF, mais la table Ensemble étant vide, il est difficile de savoir s'il faut basculer, pour ces deux sites, le champ PhaseNum vers le champ CoSiNumEns.

    SELECT `CoSiUSNum`, `PhaseNum`
    FROM `af01stratigraphie`
    WHERE `PhaseNum` != '' AND `PhaseNum` != '0' AND `PhaseNum` IS NOT NULL
    ORDER BY `CoSiUSNum` ASC

Table **af04mobilier** :
> le champ devant servir de clé primaire n'est pas unique...  
Solution : création d'un champ PK temporaire **idtemp** pour permettre de faire les liens avec les autres tables. Un nettoyage des doublons sera à réaliser ensuite.

~~1531 n°d'US sont appelés sans qu'il existe de fiche dans la table af01stratigraphie~~

> Le champ CoSiArchiv est vide. À supprimer.

> TODO : Le champ CoSepnum à lier

Table **af05ceramique** :

> pas de clé primaire.  Solution : création d'un champ **Idceram** en PK auto-incrémenté

~~2639 n° d'US sont appelés sans qu'il existe de fiche dans la table af01stratigraphie~~

> Le champ CoSiArchiv est vide. À supprimer.

> Suppression de 614 enregistrements totalement vides.

Table **af06photos** :

> 9 clés primaires dupliquées (AJ Marmoutier) à traiter manuellement après identification par requête suivante :

    SELECT COUNT(*) AS `Lignes`, `CoSiNuPho`
    FROM `af06photos`
    WHERE `Site` = "AJ"
    GROUP BY `CoSiNuPho`
    ORDER BY `Lignes` DESC
    
> Les champs **Sujet** et **NumSujet** sont à externaliser dans une table intermédiaire pour les types US, Sep, Mur et Fait ; idem pour la table **arcv_interscan** ; idem pour la table **af11dessins**

> Les champs y, z, t, zp1, zp2 et zp3 sont vides. À supprimer.

> Pas de champ Site...

Table **af08sequences** :

~~La table af01stratigraphie appelle 2940 clés primaires absentes de la table af08sequences~~

~~Le champ « couleur », vide, a été supprimé~~

> Séquence AA000673 liée à une agrégation n°-31072...

Table **af11dessins** :

> 466 clés dupliquées... Unicité par prise en compte du code de série

> Duplication pour indexation de plusieurs fait, Mur, strct... Donc à exploiter via une table de n à n

Table **af12volumes** :

> abandonner en utilisant les tables de documentation ? ainsi que af11dessins et af06photos ? Contrôler la présence éventuelle dans ``documentation`` 

Table **af13ensembles** :

~~Les quatre champs « couleur », vides, ont été supprimés~~

> TODO : mettre à jour cette table avec le tableau d'Anne-Marie Jouquand pour le site ZW  
Fichier = ``Définition des phases Poitiers Cordeliers.xlsx``

Table **af14agregations** :

~~Les champs « couleur » et « x », vides, ont été supprimés~~

Table **af15photosmobilier** :

> Correction des code « MO » en « M0 » pour les identifiants de photo

    UPDATE table SET colname = REPLACE(colname, 'oldtexte', 'newtexte')

> Mise à NULL des champs sans identifiant d'objet

> Le lien avec la table mobilier ne pourra se faire qu'après nettoyage des doublons de celle-ci.

Table **af__egalite** :

~~suppression du champ vide DoublEgal.~~

~~Ajout des US manquantes dans af01 (message = fiche manquante (présent dans relation SOUS) DONE ?~~

Table **af__equivalence** :

~~suppression du champ vide DoublEquiv.~~

~~Ajout des US manquantes dans af01 (message = fiche manquante (présent dans relation SOUS)~~

Table **af__sur** :

~~suppression du champ vide DoublSur.~~
~~Ajout des US manquantes dans af01 (message = fiche manquante (présent dans relation SUR)~~v

Table **af__sous** :

~~suppression du champ vide DoublSous.~~
~~Ajout des US manquantes dans af01 (message = fiche manquante (présent dans relation SOUS)~~

Table **dc01typodom**

~~Plusieurs numéros de Site incorrectes (3#, 33, 39, 99) : à corriger = site AA et corriger les numéros d'US des sites 33 en supprimant le 3 initial. Fiches 99 sans US à supprimer.~~

Table **dc02typinus** (table nn entre types et US)

> Numéro de site = 17 -> Marmoutier AJ + CISiUSNu

> Numéros d'US à 7 chiffres pour St-Mesme ZY (enr. n°3715) 98 en ZY !!

> TODO : 19 USNum à 0 (mais numéros présents sans CoSiUSNu

> TODO : Certains CoSiUSNu sans le code de site

> Voir enregistrement n°3578 incohérent -\> **AJ** ou AZ si on se réfère un numéro d'US

Table **dc03typfun** (table nn entre types et sépultures) 
-> ne sera pas réintégrée

> 77 enregistrements avec numéro de site vide (-> à vérifier, mais probablement Rigny ZZ, sinon St-Mesme ZY)

> CoSiUSNu non valides

> CoNumSep non valides

Table **dc04typprod** -> ne sera pas réintégrée

> Aucun numéro de site ou d'US...

Table **dc05nrpoids**

> Site n°994 ? -\> Rigny = ZZ ; 98 = ZY

> Bcp de CoSiUSNum invalides à faire au cas par cas : 976=AA ; 5113=AC ; 98=ZY ; 99=ZZ

Table **dc06cifoatt** -> énumération à gérer avec table référence indexée

~~Suppression colonnes Champ_2 et Champ_3 vides.~~

> Champ tronqué dans 4D -> à faire corriger par Cécile Bébien

> Beaucoup d'élément avec « ? » : revoir la gestion de la certitude de l'information

Table **dc07clerebo** -> énumération à gérer avec table référence indexée

~~Suppression colonne HB vide~~

Table **dc08cledeco** -> énumération à gérer avec table référence indexée

> Champ tronqué dans 4D. Peut-on récupérer l'information ?

> Bcp d'instances en doublons (par exemple 110 « DSP_rouelle à rayon/molet ») car à la création depuis la typo.-> reprendre la liste depuis la table DC01.

Table **dc09clfosup** -> énumération à gérer avec table référence indexée

> Des vrais et pseudos doublons à nettoyer, par exemple « amphore gauloise », « amph gouloise »...

Table **dc10clnuva** (numéros de vases) -> énumération à gérer avec table référence indexée

~~Suppression colonne Champ2 vide~~

> Revoir valeurs initiales (vide, 0, et caractère spécial) -> à supprimer + dans numvase de dc01

Table **dc11clegt** -> énumération à gérer avec table référence indexée

> Certains avec *

> Harmoniser les maj/minuscules

Table **dc12quanti** -> table temporaire pour le programme, mais peut-être pas à réutiliser

> Seuls GT et Nombre possèdent des informations : tous les autres champs sont vides ou 0 (à voir si nécessaire dans le programme)

Table **dc13bodagt** -> table temporaire pour les bornes calculées depuis R pour compléter ensuite dans les enregistrements

> Estim_inf et estim_sup sont systématiquement égal à zéro : à supprimer ? -> **OUI**

Table **dc14bodaen** -> bornes de datation des ensembles

> À associer à dc15 pour le num d'ensemble

> lmCourbe vide : à supprimer ?

Table **dc15datens** -> table de réception pour les calcules
temporaires

> À vider. À conserver ?

Table **dc16arsint**

> À supprimer

> Pas les bons codes de site (voir aussi champ SiteExt)

> 3 CoSiSeq en doublons. 2751 vides.

Table **dc17arsmob** (verre, déjà intégré à Mobilier)

> À supprimer

Table **dc18clegti** -> énumération à gérer avec table référence indexée

> Présence de caractères étranges...

Table **dc19categcera** -> groupes de GT

> À conserver pour faire de la quantification par catégorie ?

Table **dc20quantif2** -> table temporaire

> à supprimer ?

> 1 élément CategCera en doublon : 31.174 (sinon, clé primaire ?)

Table **dcfichimg**

> À gérer différemment pour les images des dessins de céram : à supprimer

> Présence de caractères spéciaux invisibles

> Champ HB et Image vides

Table **dcgtabs** -> table temporaire

> À supprimer ?

> Colonne NbAbs systématiquement à zéro

> GTAbs pourrait être la clé primaire ?

Tbale **JPC30Eping**

> Spécifique pour description des épingles de St-Mesme-de-Chinon. À garder ??? À intégrer dans mobilier (si pas déjà fait) ?
